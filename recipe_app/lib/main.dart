import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Recipe API',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Recipe API Example'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late Future<List<dynamic>> recipes;

  Future<List<dynamic>> getRecipes() async {
    var url = Uri.https('rest.bryancdixon.com', '/food/');
    var response = await http.get(url);
    if(response.statusCode == 200){
      var jsonResponse = jsonDecode(response.body);
      if(kDebugMode){
        print('first item in recipe list: ${jsonResponse["recipes"][0]}');
      }
      return jsonResponse['recipes'];
    } else {
      if (kDebugMode) {
        print('Response status: ${response.statusCode}');
      }
      return List.empty();
    }
  }

  Future<void> _launchUrl(Uri url) async {
    if(!await launchUrl(url)){
      if(kDebugMode){
        print('Could not launch $url');
      }
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    recipes = getRecipes();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Recipes!',
            ),
            FutureBuilder<List>(
              future: recipes,
              builder: (context, snapshot){
                switch(snapshot.connectionState){
                  case ConnectionState.waiting:
                    return const CircularProgressIndicator();
                  default:
                    if(snapshot.hasError){
                      return Text('Error: ${snapshot.error}');
                    } else {
                      //return Text(snapshot.data![0]['title']);
                      return Expanded(
                        child: ListView.builder(
                          padding: const EdgeInsets.all(8.0),
                          itemCount: snapshot.data!.length,
                          itemBuilder: (BuildContext context, int index){
                            //return Text(snapshot.data![index]['title']);
                            return GestureDetector(
                              onTap:() {
                                Uri url = Uri.parse(snapshot.data![index]['url']);
                                _launchUrl(url);
                              },
                              child: Card(
                                child: Column(
                                  children: [
                                    ListTile(
                                      title: Text(snapshot.data![index]['title']),
                                    ),
                                    Image.network(snapshot.data![index]['photo_url']),
                                  ],
                                ),
                              ),
                            );
                          }
                        ),
                      );
                    }
                }
              }
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getRecipes,
        tooltip: 'Get Recipes',
        child: const Icon(Icons.food_bank),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
