// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:counter_test/main.dart';

void main() {
  // Widget Tests
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(const MyApp());

    // Verify that our counter starts at 0.
    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // Tap the '+' icon and trigger a frame.
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    // Verify that our counter has incremented.
    expect(find.text('0'), findsNothing);
    expect(find.text('1'), findsOneWidget);
  });

  testWidgets('CINS467 text matches', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(const MyApp());

    // verifying "CINS467 Hello World" text exists
    expect(find.text('CINS467 Hello World'), findsAtLeastNWidgets(1));
  });

  testWidgets('Counter text does not go below 0', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(const MyApp());

    // verify counter starts at 0
    expect(find.text('0'), findsOneWidget);

    // tap the decrement button and trigger a frame
    await tester.tap(find.text('Decrement'));
    await tester.pump();

    // verify that the counter does NOT decrement value below 0
    expect(find.text('0'), findsOneWidget);
    expect(find.text('-1'), findsNothing);
  });

  testWidgets('Counter text gets decremented if above 0', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(const MyApp());

    // verify counter starts at 0
    expect(find.text('0'), findsOneWidget);

    // increment counter
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    // verify counter is at 1
    expect(find.text('1'), findsOneWidget);

    // tap the decrement button and trigger a frame
    await tester.tap(find.text('Decrement'));
    await tester.pump();

    // verify that the counter decrements value if above 0
    expect(find.text('0'), findsOneWidget);
  });

  // Unit tests
  test('Counter value should be incremented', () {
    final counter = Counter();

    expect(counter.value, 0);
    counter.increment();
    expect(counter.value, 1);
  });

  test('Counter value should be decremented', () {
    final counter = Counter();

    expect(counter.value, 0);
    counter.decrement();
    expect(counter.value, -1);
  });
}
