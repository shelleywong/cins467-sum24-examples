import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // TRY THIS: Try running your application with "flutter run". You'll see
        // the application has a purple toolbar. Then, without quitting the app,
        // try changing the seedColor in the colorScheme below to Colors.green
        // and then invoke "hot reload" (save your changes or press the "hot
        // reload" button in a Flutter-supported IDE, or press "r" if you used
        // the command line to start the app).
        //
        // Notice that the counter didn't reset back to zero; the application
        // state is not lost during the reload. To reset the state, use hot
        // restart instead.
        //
        // This works for code too, not just values: Most code changes can be
        // tested with just a hot reload.
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // TextField Example
  final _searchController = TextEditingController();
  String _inputText = '';

  // Form Example
  final _nameController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String _name = '';

  // // for TextField example
  // void _printLatestSearchTerm(){
  //   if(kDebugMode){
  //     print('Search field input: ${_searchController.text}');
  //   }
  // }

  // for TextField example
  void _submitText(){
    setState((){
      _inputText = _searchController.text;
    });
  }

  // Form Example
  String? _nameValidator(String? value){
    if(value == null || value.isEmpty){
      return 'Please enter your name';
    }
    if(value.contains('@')){
      return 'Name should not contain the @ char';
    }
    if(value.length < 2){
      return 'Must use at least 2 characters';
    }
    return null;
  }

  // Form Example
  void _saveName(String? value) {
    setState((){
      _name = _nameController.text;
    });
  }

  // Form Example
  void _submitForm() {
    if(_formKey.currentState!.validate()){
      if(kDebugMode){
        print('TextFormField input: ${_nameController.text}');
      }
      _formKey.currentState!.save();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: const Text('Form Submitted Successfully!'),
          action: SnackBarAction(
            label: 'Finish',
            onPressed: () {
              _nameController.clear();
            }
          )
        )
      );
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //_searchController.addListener(_printLatestSearchTerm);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _searchController.dispose();
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // TRY THIS: Try changing the color here to a specific color (to
        // Colors.amber, perhaps?) and trigger a hot reload to see the AppBar
        // change color while the other colors stay the same.
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text('Form Example:'),
            Form(
              key: _formKey,
              child: Column(
                children: [
                  TextFormField(
                    controller: _nameController,
                    decoration: const InputDecoration(
                      icon: Icon(Icons.person),
                      labelText: 'Name *',
                      hintText: 'What do people call you?',
                    ),
                    validator: _nameValidator,
                    onSaved: _saveName,
                  ),
                  ElevatedButton(
                    onPressed: _submitForm,
                    child: const Text('Submit Name'),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(_name),
            ),
            const SizedBox(height: 100),
            const Text('TextField Example:'),
            TextField(
              controller: _searchController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Enter a search term',
                hintText: 'Search...',
              )
            ),
            ElevatedButton(
              onPressed: _submitText,
              child: const Text('Search'),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(_inputText),
            ),
          ],
        ),
      ),
    );
  }
}
