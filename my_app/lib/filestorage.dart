import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';

// Ref: https://docs.flutter.dev/cookbook/persistence/reading-writing-files
class FileStorage {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/input.txt');
  }

  Future<void> writeCounter(int newCounter) async {
    try {
      final file = await _localFile;
      String jsonString = json.encode({'counter': newCounter});
      // Write the file
      file.writeAsString(jsonString);
    } catch(e) {
      if (kDebugMode) {
        print('writeCounter error: $e');
      }
    }
  }

  Future<int> readCounter() async {
    try {
      final file = await _localFile;

      // Read the file
      final contents = await file.readAsString();
      var counterData = json.decode(contents);
      return counterData['counter'];
      //return int.parse(contents);
    } catch (e) {
      if (kDebugMode) {
        print('readCounter error: $e');
      }
      // If encountering an error, return 0
      return 0;
    }
  }
}