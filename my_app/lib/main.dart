import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:go_router/go_router.dart';
import 'package:uuid/uuid.dart';
import 'package:firebase_storage/firebase_storage.dart';

import 'storage.dart';
import 'firebase_options.dart';
import 'photos.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  runApp(const MyApp());
}

final _router = GoRouter(
  routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => kIsWeb
        ? const MyHomePage(title: 'CINS467 Web')
        : (Platform.isAndroid ? const MyHomePage(title: 'CINS467 Android') : const MyHomePage(title: 'CINS467')),
    ),
    GoRoute(
      path: '/photos',
      builder: (context, state) => PhotosPage(),
    ),
  ],
);

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.purple),
        useMaterial3: true,
      ),
      routerConfig: _router,
      // home: kIsWeb
      //   ? const MyHomePage(title: 'CINS467 Web')
      //   : (Platform.isAndroid ? const MyHomePage(title: 'CINS467 Android') : const MyHomePage(title: 'CINS467')),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // firebase (cloud firestore) example
  final Storage _storage = Storage();
  int _counter = 0;

  late Future<Position> _position;
  final LocationSettings _locationSettings = const LocationSettings(
    accuracy: LocationAccuracy.high,
    distanceFilter: 100,
  );
  late StreamSubscription<Position> _positionStream;

  File? _image; // for android
  String? _imagePath; // for web
  Uint8List? _imageForWeb; // for web

  Future<void> _upload() async {
    if(_image != null || _imagePath != null){
      // Generate a v4 (random) id (unique identifer)
      const uuid = Uuid();
      final String uid = uuid.v4();
      // Upload the image file to storage & generate url for database
      final String downloadURL = await _uploadFile(uid);
      // Add url (ref to the image) and title to the database
      await _addItem(downloadURL, uid);
      // Navigate to photos screen
      if(mounted){
        context.go('/photos');
      }
    }
  }

  // Adding photo file to Firebase/Google Cloud Storage
  Future<String> _uploadFile(String filename) async {
    // create a reference to a file location in Storage
    Reference ref = FirebaseStorage.instance.ref().child('$filename.jpg');
    // Add metadata to the image file
    final metadata = SettableMetadata(
      contentType: 'image/jpeg',
      contentLanguage: 'en',
    );
    if(kIsWeb){
      try {
        // Upload raw data for web image
        TaskSnapshot uploadTask = await ref.putData(_imageForWeb!, metadata);
        final String downloadURL = await uploadTask.ref.getDownloadURL();
        return downloadURL;
      } on FirebaseException catch(e) {
        return '_uploadFile error (web): $e';
      }
    } else {
      // Upload the file to Storage (android)
      final UploadTask uploadTask = ref.putFile(_image!, metadata);
      // After the upload is complete, get a url and return
      // the url to be used in the database entry
      TaskSnapshot uploadResult = await uploadTask;
      final String downloadURL = await uploadResult.ref.getDownloadURL();
      return downloadURL;
    }
  }

  // Add entry to Cloud Firestore database (photos collection)
  Future<void> _addItem(String downloadURL, String title) async {
    await FirebaseFirestore.instance
      .collection('photos')
      .add(<String, dynamic>{
        'downloadURL': downloadURL,
        'title': title,
        'timestamp': DateTime.now(),
      });
  }

  Future<void> _getImage() async {
    final ImagePicker picker = ImagePicker();
    // Capture a photo with the camera
    final XFile? photo = await picker.pickImage(source: ImageSource.camera);
    if(photo != null){
      if(kIsWeb){
        _imageForWeb = await photo.readAsBytes();
        setState((){
          _imagePath = photo.path;
        });
      } else {  // Android/mobile
        setState((){
        _image = File(photo.path);
      });
      }
    } else {
      if(kDebugMode){
        print('No photo captured');
      }
    }
  }

  /// Determine the current position of the device.
  ///
  /// When the location services are not enabled or permissions
  /// are denied the `Future` will return an error.
  /// Ref: https://pub.dev/packages/geolocator
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the 
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale 
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }
    
    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately. 
      return Future.error(
        'Location permissions are permanently denied, we cannot request permissions.');
    } 

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  Future<void> _incrementCounter() async {
    await _storage.readCounter().then((value) async {
      final int counter = value + 1;
      await _storage.writeCounter(counter);
      setState(() {
        _counter = counter;
      });
    });
  }

  Future<void> _decrementCounter() async {
    await _storage.readCounter().then((value) async {
      final int counter = value - 1;
      await _storage.writeCounter(counter);
      setState(() {
        _counter = counter;
      });
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // _counter = _prefs.then((SharedPreferences prefs){
    //   return prefs.getInt('counter') ?? 0;
    // });
    _storage.readCounter().then((value) {
      setState(() {
        _counter = value;
      });
    });
    _position = _determinePosition();

    _positionStream = Geolocator.getPositionStream(locationSettings: _locationSettings).listen((Position? pos){
      // handle position changes
      if(kDebugMode){
        print(pos == null ? 'Unknown' : '${pos.latitude}, ${pos.longitude}');
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _positionStream.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        title: Text(
          widget.title,
          style: const TextStyle(color: Colors.white),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.photo_album, color: Colors.white),
            tooltip: 'See Photo Collection',
            onPressed: () {
              context.go('/photos');
            },
          ),
        ],
      ),
      body: Center(
        child: ListView(
          padding: const EdgeInsets.all(8.0),
          children: <Widget>[
            _imagePath == null // for web
            ? const SizedBox.shrink()
            : Image.network(_imagePath!),
            _image == null // for Android
             //? const Icon(Icons.photo, size: 100)
             ? const SizedBox.shrink()
             : Image.file(_image!, height: 200),
            Tooltip(
              message: kIsWeb ? 'Pick from Gallery' : 'Launch the camera',
              child: ElevatedButton(
                onPressed: _getImage,
                child: const Icon(Icons.photo_camera),
              ),
            ),
            ElevatedButton(
              onPressed: _upload,
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.indigo,
              ),
              child: const Text(
                'Upload Photo',
                style: TextStyle(fontSize: 20, color: Colors.white),
              )
            ),
            FutureBuilder<Position>(
              future: _position,
              builder: (context, snapshot) {
                switch(snapshot.connectionState){
                  case ConnectionState.waiting:
                    return const CircularProgressIndicator();
                  default:
                    if(snapshot.hasError){
                      return Text('Location error: ${snapshot.error}');
                    } else {
                      return Text(
                        'Latitude: ${snapshot.data?.latitude}, Longitude: ${snapshot.data?.longitude}, Accuracy: ${snapshot.data?.accuracy}'
                      );
                    }
                }
              }
            ),
            Container(
              //alignment: Alignment.center,
              constraints: const BoxConstraints(maxHeight: kIsWeb ? 1000 : 300),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(30.0),
                child: const Image(
                  image: AssetImage('assets/chicostateflowers.jpeg'),
                ),
              ),
            ),
            const Center(
              child: Text(
                'You have pushed the button this many times:',
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Tooltip(
                    message: 'Decrement Counter',
                    child: ElevatedButton(
                      onPressed: _decrementCounter,
                      style: ElevatedButton.styleFrom(
                          backgroundColor:
                              Theme.of(context).colorScheme.inversePrimary),
                      //child: const Text('Decrement'),
                      child: const Icon(Icons.arrow_downward),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Count: $_counter',
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                  ),
                ),
                // FutureBuilder<int>(
                //   future: _counter,
                //   builder: (context, snapshot) {
                //     switch(snapshot.connectionState){
                //       case ConnectionState.waiting:
                //         return const CircularProgressIndicator();
                //       default:
                //         if(snapshot.hasError){
                //           return Text('Error: ${snapshot.error}');
                //         } else {
                //           return Expanded(
                //             flex: 2,
                //             child: Padding(
                //               padding: const EdgeInsets.all(8.0),
                //               child: Text(
                //                 'Count: ${snapshot.data}',
                //                 style: Theme.of(context).textTheme.headlineMedium,
                //               ),
                //             ),
                //           );
                //         }
                //     }
                //   }
                // ),
                Expanded(
                  child: IconButton(
                    icon: const Icon(Icons.add_circle),
                    tooltip: 'Increment the Counter',
                    onPressed: _incrementCounter,
                  ),
                ),
              ],
            ),
            StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection('greetings')
                    .snapshots(),
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return const CircularProgressIndicator();
                    default:
                      if (snapshot.hasError) {
                        return Text('Error: ${snapshot.error}');
                      } else if (!snapshot.hasData) {
                        return const SizedBox.shrink();
                      } else {
                        return ListView.builder(
                          shrinkWrap: true,
                            itemCount: snapshot.data!.docs.length,
                            itemBuilder: (context, index) {
                              return Text(
                                '${snapshot.data!.docs[index]["message"]}',
                                textAlign: TextAlign.center,
                                style: Theme.of(context).textTheme.displaySmall,
                              );
                            });
                      }
                  }
                }),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
