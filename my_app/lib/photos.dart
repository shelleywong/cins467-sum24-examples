// import 'dart:async';
// import 'dart:io';

//import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
//import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:image_picker/image_picker.dart';
import 'package:go_router/go_router.dart';

class PhotosPage extends StatelessWidget {
  PhotosPage({ super.key });
  final myScrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        title: const Text(
          'Photos!',
          style: TextStyle(color: Colors.white),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.home, color: Colors.white),
            tooltip: 'Go Home',
            onPressed: () {
              context.go('/');
            },
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: getBody(),
        )
      )
    );
  }

  List<Widget> getBody() {
    return [
      const Text('We made it!'),
      StreamBuilder(
        stream: FirebaseFirestore.instance.collection('photos').snapshots(),
        builder: (context, snapshot){
          switch(snapshot.connectionState){
            case ConnectionState.waiting:
              return const CircularProgressIndicator();
            default:
              if(snapshot.hasError){
                return Text('Error: ${snapshot.error}');
              } else {
                return Expanded(
                  child: Scrollbar(
                    controller: myScrollController,
                    child: ListView.builder(
                      controller: myScrollController,
                      //shrinkWrap: true,
                      physics: const AlwaysScrollableScrollPhysics(),
                      itemCount: snapshot.data!.docs.length,
                      itemBuilder: (context, index){
                        return photoWidget(snapshot, index);
                      }
                    ),
                  ),
                );
              }
          }
        },
      )
    ];
  }

  Widget photoWidget(AsyncSnapshot<QuerySnapshot> snapshot, int index){
    try {
      return Column(
        children: [
          ListTile(
            leading: const Icon(Icons.person),
            title: Text(snapshot.data!.docs[index]['title']),
            subtitle: snapshot.data!.docs[index].data().toString().contains('timestamp') 
              ? Text(DateTime.fromMillisecondsSinceEpoch(snapshot.data!.docs[index]['timestamp'].seconds * 1000).toString())
              : const Text(''),
          ),
          Image.network(snapshot.data!.docs[index]['downloadURL']),
        ]
      );
    } catch(e){
      return Text('Error: $e');
    }
  }

}

