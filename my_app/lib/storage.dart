import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Storage {

  Future<void> writeCounter(int newCounter) async {
    try {
      FirebaseFirestore firestore = FirebaseFirestore.instance;
      firestore.collection('counterCollection')
        .doc('cins467')
        .set({'count': newCounter})
        .then((value){
          if (kDebugMode) {
            print('count updated successfully');
          }
        }).catchError((error){
          if (kDebugMode) {
            print('Error calling counterCollection set method');
          }
        });
    } catch(e) {
      if (kDebugMode) {
        print('writeCounter error: $e');
      }
    }
  }

  Future<int> readCounter() async {
    try {
      FirebaseFirestore firestore = FirebaseFirestore.instance;
      DocumentSnapshot ds = await firestore.collection('counterCollection')
        .doc('cins467')
        .get();
      if(ds.data() != null){
        Map<String, dynamic> data = (ds.data() as Map<String, dynamic>);
        if(data.containsKey('count')){
          return data['count'];
        }
      }
      return 0;
    } catch (e) {
      if (kDebugMode) {
        print('readCounter error: $e');
      }
      // If encountering an error, return 0
      return 0;
    }
  }
}