import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

Widget getDrawer(BuildContext context) {
  return Drawer(
    child: ListView(
      children: [
        const DrawerHeader(
          decoration: BoxDecoration(
            color: Colors.deepPurple,
          ),
          child: Text('CINS467'),
        ),
        ListTile(
          leading: const Icon(Icons.home),
          title: const Text('Home'),
          onTap: () {
            context.go('/');
          }
        ),
        ListTile(
          leading: const Icon(Icons.face),
          title: const Text('Second Page'),
          onTap: () {
            context.go('/page2');
          }
        ),
      ],
    )
  );
}
