import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import 'first.dart';
import 'second.dart';

// GoRouter configuration
final _router = GoRouter(
  routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => const FirstRoute(),
    ),
    GoRoute(
      path: '/page2',
      builder: (context, state) => const SecondRoute(),
    ),
  ],
);
void main() {
  runApp(MaterialApp.router(
    title: 'Navigation Basics',
    //home: FirstRoute(),
    routerConfig: _router,
  ));
}


